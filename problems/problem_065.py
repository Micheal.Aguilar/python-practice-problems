# Write a function that meets these requirements.
#
# Name:       biggest_gap
# Parameters: a list of numbers that has at least
#             two numbers in it
# Returns:    the largest gap between any two
#             consecutive numbers in the list
#             (this will always be a positive number)
#
# Examples:
#     * input:  [1, 3, 5, 7]
#       result: 2 because they all have the same gap
#     * input:  [1, 11, 9, 20, 0]
#       result: 20 because from 20 to 0 is the biggest gap
#     * input:  [1, 3, 100, 103, 106]
#       result: 97 because from 3 to 100 is the biggest gap
#
# You may want to look at the built-in "abs" function

def biggest_gap(nums):
  max_gap = abs(nums[1]- nums[0]) # 'abs'(absolute value) of the values of num[1] - num[2] and assign it to max_gap
  for i in range(1,len(nums) -1): # looping through items in the range between the min and max number u assign it to. starts at num1 and returns the number of items in the parameter then signs the first item from the last item in the paramter
    gap = abs(nums[1+1]- nums[i]) # 'abs'(absolute value) of the values of num[1 + 1] - num[i] and assign it to gap
    if gap > max_gap: # condition if gap is greater than max_gap excute the code
      max_gap = gap #assigns gap to max_gap
  return max_gap
print(biggest_gap([1,5,67,68]))
