# Write a function that meets these requirements.
#
# Name:       temperature_differences
# Parameters: highs: a list of daily high temperatures
#             lows: a list of daily low temperatures
# Returns:    a new list containing the difference
#             between each high and low temperature
#
# The two lists will be the same length
#
# Example:
#     * inputs:  highs: [80, 81, 75, 80]
#                lows:  [72, 78, 70, 70]
#       result:         [ 8,  3,  5, 10]


def temperature_differences(highs,lows):
    differences = [] # creating a new list for the outputs
    for high, low in zip(highs, lows): # looping through a 2 list for high and low and 'zip'('adds list1[1] and list2[1] together'If one tuple contains more items, these items are ignored.)
      differences.append(high-low) # in the new_list we are '-' the highs and lows and putting them in there.
    return differences # returning the value
print(temperature_differences([56,67,43,21],[87,98,32,65]))
