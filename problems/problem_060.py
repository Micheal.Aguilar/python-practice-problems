# Write a function that meets these requirements.
#
# Name:       only_odds
# Parameters: a list of numbers
# Returns:    a copy of the list that only includes the
#             odd numbers from the original list
#
# Examples:
#     * input:   [1, 2, 3, 4]
#       returns: [1, 3]
#     * input:   [2, 4, 6, 8]
#       returns: []
#     * input:   [1, 3, 5, 7]
#       returns: [1, 3, 5, 7]


# def only_odds(nums):  #Function
#   output = []
#     # new list for the outputs
#   for num in nums:                                                   # the for loop iterating through a number in numbers
#     if num % 2 == 1:
#         # if the number is divisble 2 and has a remainder of 1
#       output.append(num)
#         # going to put the number is the output.
#   return output
#     # going to return the output.
# print(only_odds([7,5,6,54]))


def only_odds(nums):
    output = []
    for num in nums:
        if num % 2 == 1:
            output.append(num)
    return output
print(only_odds([4,7,6,44]))
