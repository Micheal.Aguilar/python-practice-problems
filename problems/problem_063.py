# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_letters (word):
  new_word = '' # output of new string
  for letter in word: # looping through each letter in the word parameter
    if letter == 'Z': # if letter is equal to uppercase Z
      new_letter = 'A' # changes to uppercase A
    elif letter == 'z': # if letter is equal to lowercase z
      new_letter = 'a' # changes to lowercase a
    else:
      new_letter = chr(ord(letter) + 1) # chr('get the unicode that represents that letter)ord('return char to integer)
    new_word += new_letter # adds a new_letter to new_word and reassigns it to new_word
  return new_word # is to exit a function and return a value

print(shift_letters('Micheal'))
