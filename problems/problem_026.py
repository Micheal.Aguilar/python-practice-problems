# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):

    if len(values) == 0:
        return None
    sum = 0
    for item in values:
        sum = sum + item
    average = sum / len(values)
    if sum >= 90 :
        return "A"
    elif sum >= 80 and sum <= 90:
        return "B"
    elif sum >= 70 and sum <=80:
        return "C"
    elif sum >= 60 and sum <=70:
        return "D"
    else:
        return "F"
print(calculate_grade([80]))
