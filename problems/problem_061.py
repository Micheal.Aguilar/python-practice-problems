# Write a function that meets these requirements.
#
# Name:       remove_duplicates
# Parameters: a list of values
# Returns:    a copy of the list removing all
#             duplicate values and keeping the
#             original order
#
# Examples:
#     * input:   [1, 1, 1, 1]
#       returns: [1]
#     * input:   [1, 2, 2, 1]
#       returns: [1, 2]
#     * input:   [1, 3, 3, 20, 3, 2, 2]
#       returns: [1, 3, 20, 2]


def remove_duplicates(values):
  output = [] # have to creat a new list without duplicates
  for value in values: # looping through the values in the values parameter
    if value not in output: # if value is 'not in'(boolean Operator) in the ouput
        output.append(value) # adding  the new value at the end of the output array[]
  return output
print(remove_duplicates([1,3,3,20,3,2,2]))
